var Ajedrez = (function(){
    var _mostrar_tablero = function(){
        peticion();
    }
    var peticion = function(){
        var url =  "csv/tablero.csv";
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
          if (this.readyState === 4) {
            if (this.status === 200) {
                verificarIntegridadArchivo(this.responseText);
            } else {
                mostrarMsj("Error " + this.status + " " + this.statusText + " - " + this.responseURL);
            }
          }
        };
        xhr.open('GET', url, true);
        xhr.send();            
        xhr.onerror=function(err){
            console.log(err);
             mostrarMsj("Error al contactar con el servidor");
        };
    }

    var verificarIntegridadArchivo = function(archivo){
        var filas = archivo.split('\n');
        if(filas.length!=9)
        {
            mostrarMsj("El archivo no posee 9 filas");
            return;
        }
        else{
            var msj ="";
            for(var i =0;i<9;i++)
            {
                var columnas = filas[i].split('|');
                if(columnas.length!=8){
                    msj+="El archivo no posee 8 columnas en la fila "+i+" contiene "+columnas.length+"\n";
                }
            }
            if(msj!==""){
                mostrarMsj(msj);
                return;
            }
        }
        rellenar(archivo);
    }

    var mostrarMsj = function(msg){
        var mensaje = document.getElementById("mensaje");
        mensaje.innerText="";
        mensaje.innerText= msg;
    }
    var rellenar = function(archivo){
        var tablero = document.getElementById("tablero");
        tablero.innerHTML = "";
        var filas = archivo.split('\n');
        var letras = filas[0].split('|');
   
        var color = false;
        for(var i=0;i<9;i++)
        {
            var espacio = document.createElement("div");
            espacio.classList.add("casilla");
            espacio.innerText = i!=0?(9-i):"";
            tablero.appendChild(espacio);

            var columna = filas[i].split('|');
            for(var j  = 0;j<letras.length;j++)
            {
                var casilla = document.createElement("div");
                casilla.classList.add("casilla");
                if(i!=0){
                    casilla.id = letras[j]+(9-i);  
                    if(color)
                        casilla.classList.add("clara");
                    else
                    casilla.classList.add("obscura");
                }
                casilla.innerText = columna[j]!=="∅"?columna[j]:"";
                tablero.appendChild(casilla);
                color = !color;
            }
            color = !color;
        }  
    }
    var _actualizar_tablero = function(){
        mostrarMsj("");
        peticion();
    }
    var validarEntrada = function(objetoE){
        //console.log(objetoE);
        if(typeof objetoE !== 'object' || objetoE ===null||Array.isArray(objetoE)){
            mostrarMsj("El parametro debe ser un objeto de la forma\n { \"de\": posicion, \"a\": posicion}");
            return;
        }
        if(objetoE.de==undefined){
            mostrarMsj("El parametro \"de\"  no esta definido");
            return;
        }
        if(objetoE.a==undefined){
            mostrarMsj("El parametro \"a\" no esta definido");
            return;
        }
        var de = document.getElementById(objetoE.de);
        var a = document.getElementById(objetoE.a); 
        if(de==undefined){
            mostrarMsj("El parametro \"de\"  no corresponde a una posición válida");
            return;
        }else if (a== undefined)
        {
            mostrarMsj("El parametro \"a\" no corresponde a una posición válida");
            return;
        }
        actualizarInterfaz(de,a);
    }
    var actualizarInterfaz = function(de,a){
        if(de.innerText!=""){
            if(a.innerText==""){
                a.innerText = de.innerText;
                de.innerText ="";
            }
            else{
                mostrarMsj("La casilla destino se encuentra ocupada");
                return;    
            }
        }
        else{
            mostrarMsj("No hay ficha en la posicion de origen");
            return;
        }
    }
    var _mover_pieza = function(movimiento){
        mostrarMsj("");
        validarEntrada(movimiento);        
    }

    return {
        "mostrarTablero": _mostrar_tablero,
        "actualizarTablero": _actualizar_tablero,
        "moverPieza":_mover_pieza
    }
})();